/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeux_allumettes;

import java.util.ArrayList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Peterson Yuhala
 */
public class GameStage extends Stage {

    private Button play, newGame;
    public TextField sticks;
    public Label sticksLeft, move, maxTime, minTime, numSticks, choice, timer;
    public Stage stage;
    public Game game;

    public GameStage(Stage owner, boolean modality, Game g) throws Exception {
        initOwner(owner);
        stage = owner;
        this.game = g;
        owner.hide();
        Modality m = modality ? Modality.APPLICATION_MODAL : Modality.NONE;
        initModality(m);

        play = new Button("JOUER");
        newGame = new Button("NOUVEAU JEUX");

        //Set button actions
        play.setOnAction(e -> play());
        newGame.setOnAction(e -> newGame());

        //Definitions des styles CSS sur les differents elements
        String img = GameStage.class.getResource("/img/CoarseGrid.png").toExternalForm();
        String styleLabel = "-fx-font-family: \"Arial\";\n"
                + "    -fx-font-size: 18;\n"
                + "    -fx-text-fill: white;\n"
                + "    -fx-effect: dropshadow( one-pass-box , rgba(0, 0, 0, 0.6), 0, 0.0 , 0 , 1 );";
        String stickLabel = "-fx-font-family: \"Arial\";\n"
                + "    -fx-font-size: 25;\n"
                + "    -fx-text-fill: blue;\n"
                + "    -fx-effect: dropshadow( one-pass-box , rgba(0, 0, 0, 0.6), 0, 0.0 , 0 , 1 );";

        String paneStyle = "-fx-background-image: url('" + img + "');"
                + "    -fx-background-repeat: repeat;\n"
                + "    -fx-background-color:\n"
                + "        linear-gradient(#38424b 0%, #1f2429 20%, #191d22 100%),\n"
                + "        linear-gradient(#20262b, #191d22),\n"
                + "        radial-gradient(center 50% 0%, radius 100%, rgba(114,131,148,0.9), rgba(255,255,255,0));";

        String buttonStyle = "-fx-background-color:\n"
                + "        rgba(255, 255, 255, 0.08),\n"
                + "        rgba(0, 0, 0, 0.8),\n"
                + "        #090a0c,\n"
                + "        linear-gradient(#4a5661 0%, #1f2429 20%, #1f242a 100%),\n"
                + "        linear-gradient(#242a2e, #23282e),\n"
                + "        radial-gradient(center 50% 0%, radius 100%, rgba(135,142,148,0.9),\n"
                + "        rgba(255,255,255,0));\n"
                + "    -fx-background-radius: 7, 6, 5, 4, 3, 5;\n"
                + "    -fx-background-insets: -3 -3 -4 -3, -3, 0, 1, 2, 0;\n"
                + "    -fx-font-family: \"Arial\";\n"
                + "    -fx-text-fill: white;\n"
                + "    -fx-font-size: 20;\n"
                + "    -fx-text-fill: linear-gradient(white, #d0d0d0);\n"
                + "    -fx-padding: 10 20 10 20;";
        String textFieldStyle = "-fx-font-size: 18;\n"
                + "    -fx-background-color:\n"
                + "        rgba(255, 255, 255, 0.3),\n"
                + "        linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.8) 50%),\n"
                + "        rgb(218, 226, 224);\n"
                + "    -fx-background-insets: 0 0 -1 0, 0, 1.5;\n"
                + "    -fx-background-radius: 6, 5, 4;\n"
                + "    -fx-padding: 6 10 4 10;\n"
                + "    -fx-effect: innershadow( gaussian, rgba(0, 0, 0, 0.8), 5, 0, 0, 2 );\n"
                + "    -fx-font-family: \"Arial\";";

        sticks = new TextField();
        sticks.setStyle(textFieldStyle);
        sticksLeft = new Label("ALLUMETTES RESTANTS");
        sticksLeft.setStyle(styleLabel);
        numSticks = new Label("" + this.game.sticksLeft + "");
        numSticks.setStyle(stickLabel);
        timer = new Label();
        move = new Label();
        move.setStyle(styleLabel);
        maxTime = new Label();
        minTime = new Label();
        choice = new Label("ENTRER LE NOMBRE D'ALLUMETTES A RAMASSER");
        choice.setStyle(styleLabel);
        play.setStyle(buttonStyle);
        newGame.setStyle(buttonStyle);
        GridPane pane = new GridPane();
        GridPane.setHalignment(newGame, HPos.RIGHT);
        GridPane.setHalignment(play, HPos.LEFT);
        GridPane.setHalignment(sticksLeft, HPos.LEFT);
        GridPane.setHalignment(numSticks, HPos.RIGHT);
        GridPane.setValignment(timer, VPos.TOP);
        GridPane.setValignment(move, VPos.BOTTOM);
        pane.setStyle(paneStyle);

        pane.setAlignment(Pos.CENTER);
        pane.setPadding(new Insets(20));
        pane.setHgap(15);
        pane.setVgap(15);

        pane.add(sticksLeft, 2, 1);
        pane.add(numSticks, 2, 1);
        pane.add(timer, 2, 0);
        pane.add(move, 2, 0);
        pane.add(choice, 2, 3);
        pane.add(sticks, 2, 4);
        pane.add(play, 2, 6);
        pane.add(newGame, 2, 6);

        Scene scene = new Scene(pane, 700, 550);
        setTitle("Jeux D'allumettes");
        setScene(scene);
        show();

    }

    public void play() {
        int val = 0;
        val = Integer.parseInt(this.sticks.getText());
        if (val < 0 || val > 3 || val > this.game.sticksLeft) {
            errorAlert();
        } else {
            if (this.game.algorithm == 1) {

                game.minPlay(game.minimaxState, val);
                game.maxPlay(game.minimaxState);
                this.move.setText("ORDINATEUR PREND " + this.game.sticksPicked + " ALLUMETTES");
                this.numSticks.setText("" + this.game.sticksLeft + "");
                if (this.game.sticksLeft < 2) {
                    this.game.winnerAlert(game.minimaxState);
                }

            } else {

                game.minPlay(game.abState, Integer.parseInt(this.sticks.getText()));
                game.maxPlay(game.abState);
                this.move.setText("ORDINATEUR PREND " + this.game.sticksPicked + " ALLUMETTES");
                this.numSticks.setText("" + this.game.sticksLeft + "");
                if (this.game.sticksLeft < 2) {
                    this.game.winnerAlert(game.abState);
                }

            }
        }

    }

    public void newGame() {
        this.stage.show();
        close();

    }

    public void errorAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("MAUVAISE VALEUR ENTREE!");
        alert.setContentText("ENTRER UNE VALUER LEGAL D'ALLUMETTES! ");
        alert.showAndWait();
    }

}
