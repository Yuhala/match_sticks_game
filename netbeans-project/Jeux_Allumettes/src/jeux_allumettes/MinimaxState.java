/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeux_allumettes;

import java.util.ArrayList;

/**
 *
 * @author Peterson Yuhala
 */
public class MinimaxState {
    /*
     Les objets MinimaxState represente les differents etats possible du jeux. On peut voit un etat comme un noeud 
     de l'arbre de jeux
     */

    public int player;
    public ArrayList<MinimaxState> children = new ArrayList<>();
    private int minimaxVal;
    public int numSticks;
    public int level;
    public int rest;

    public MinimaxState(int player, int num, int lev) {
        this.player = player;
        this.level = lev;
        this.numSticks = num;
        this.rest = 0;
        this.addnextMinimaxStates();
        this.minimaxVal = minimax(this, this.player);

    }

    public int getMinimaxVal() {
        return this.minimaxVal;
    }


    /*
     La fonction suivant est la fonction minimax et c'est le coeur du programme,car c'est elle qui attribue à 
     chaque etat sa valeur minimax 
     */
    public int minimax(MinimaxState state, int player) {

        /*
         Ici, si le nombre d'allumettes restant est 0 ou 1, on se trouve forcement sur un noeud terminal
         Notre fonction d'évaluation renvoi 1 au cas ou c'est MAX le joueur courant et -1 si c'est MIN
        
         */
        if (state.numSticks == 0 && player == -1) {

            return -1;
        } else if (state.numSticks == 0 && player == 1) {

            return 1;
        } else if (state.numSticks == 1 && player == -1) {
            return 1;
        } else if (state.numSticks == 1 && player == 1) {
            return -1;
        } /*
         Si on ne se trouve pas sur un noeud terminal, on calcul le min de ses enfants si c'est MAX le joueur courant.
         Dans le cas contraire, si c'est MIN qui est censé joueur sur cet noeud,on calcul le min de ses enfants
         Sela se fait enfait de façon recursive
         */ else {
            ArrayList<Integer> list = new ArrayList<>();

            for (int i = 0; i < state.children.size(); i++) {
                state.children.get(i).minimaxVal = minimax(state.children.get(i), state.children.get(i).player);
                list.add(state.children.get(i).minimaxVal);
            }

            if (player == 1) {

                return this.max(list);
            } else {
                return this.min(list);
            }
        }

    }


    /*
     La fonction addnextMinimaxStates suivant détermine les etat suivants d'un etat donné. ie Le fils d'un
     noeud;ça ajoute ensuite ces fils dans l'attribut "children" du noeud
    
     */
    public void addnextMinimaxStates() {
        int num;

        num = this.numSticks;
        int nextplayer, i = 1;//i is a counter that ensures that at most 3 sticks are taken
        if (this.player == 1) {
            nextplayer = -1;
        } else {
            nextplayer = 1;
        }
        while (num > 0 && i <= 3) {
            num--;

            MinimaxState newMinimaxState = new MinimaxState(nextplayer, num, this.level + 1);
            this.children.add(newMinimaxState);
            i++;
        }

    }

    /*
     *Les fonctions min et max suivants calcule le minimum et le maximum respectivement d'une liste d'entiers 
     passé en paramètre dans un arraylist
     Ces fonctions sont utiles au niveau ou on doit determiner les min/max des noeuds(ou etat dans notre cas)
     */
    public int min(ArrayList<Integer> list) {
        int val = 10;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) < val) {
                val = list.get(i);
            }
        }

        return val;
    }

    public int max(ArrayList<Integer> list) {
        int val = -10;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > val) {
                val = list.get(i);
            }
        }

        return val;
    }

    public int max(int a, int b) {
        return (a > b) ? a : b;
    }

    public int min(int a, int b) {
        return (b > a) ? a : b;
    }

}
