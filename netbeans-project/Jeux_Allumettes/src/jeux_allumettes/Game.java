/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeux_allumettes;

import javafx.scene.control.Alert;

/**
 *
 * @author Peterson Yuhala
 */
public class Game {
    /*
     Un objet Game represente un jeux et est representé par les attributs suivants: 
    
     */

    public AlphaBetaState abState; //L'etat present du jeux pour l'algo alpha-beta
    public MinimaxState minimaxState; //L'etat present du jeux pour l'algo Minimax
    public int presentPlayer; //Le joueur present du jeux
    public int sticksLeft; //Le nombre d'allumettes restant
    public int sticksPicked; //Le nombre d'allumettes que l'ordinateur (ie MAX) a décidé de ramasser
    public int algorithm = 1; //Choisit quelle algo a utiliser ie minimax ou alpha-beta. NB minimax=1;alpha-beta=2
    public int time;
    public int rest;
    public int given;

    public Game(int num, int algo) {
        this.algorithm = algo;       
       
        this.sticksLeft = num;
        this.sticksPicked = 0;
        this.rest = 0;
        this.given = this.sticksLeft;
        
        this.presentPlayer = -1;
        if (algo == 1) {

            this.minimaxState = new MinimaxState(this.presentPlayer, this.given, 0);

        } else {

            this.abState = new AlphaBetaState(this.presentPlayer, this.given, 0);

        }

    }

//MAX  est l'ordinateur dans notre cas et essaye de maximiser ses chances pour gagner en utilisant l'IA   
    public void maxPlay(MinimaxState state) {
        MinimaxState newState = state;
        

        for (int i = 0; i < state.children.size(); i++) {
            if (state.children.get(i).getMinimaxVal() == state.getMinimaxVal()) {
                newState = state.children.get(i);
                this.sticksPicked = state.numSticks - newState.numSticks;
            }
        }
        this.minimaxState = newState;
        this.sticksLeft = newState.numSticks;
        this.rest=0;
        this.presentPlayer = newState.player;

    }

    public void maxPlay(AlphaBetaState state) {
        AlphaBetaState newState = state;

        for (int i = 0; i < state.children.size(); i++) {
            if (state.children.get(i).getMinimaxVal() == state.getMinimaxVal()) {
                newState = state.children.get(i);
                this.sticksPicked = state.numSticks - newState.numSticks;
            }
        }
        this.abState = newState;
        this.sticksLeft = newState.numSticks;
        this.presentPlayer = newState.player;

    }

    //MIN est l'humain(joueur adversaire) dans notre cas.Il doit entrer le nombre d'allumettes à ramasser
    public void minPlay(MinimaxState state, int num) {
        MinimaxState newState = state;
        
        int sticks = this.sticksLeft - num;
        for (int i = 0; i < state.children.size(); i++) {
            if (state.children.get(i).numSticks == sticks) {
                newState = state.children.get(i);
                break;
            }
        }
        this.minimaxState = newState;
        this.sticksLeft = newState.numSticks;
        this.rest=0;
        this.presentPlayer = newState.player;
    }

    public void minPlay(AlphaBetaState state, int num) {
        AlphaBetaState newState = state;
        int sticks = this.sticksLeft - num;
        for (int i = 0; i < state.children.size(); i++) {
            if (state.children.get(i).numSticks == sticks) {
                newState = state.children.get(i);
                break;
            }
        }
        if (newState == state) {
            newState.addnextStates();
            minPlay(newState, num);
        }
        this.abState = newState;
        this.sticksLeft = newState.numSticks;
        this.presentPlayer = newState.player;

    }

    public void winnerAlert(MinimaxState state) {
        String string = "";
        if (state.player == 1 && state.numSticks == 0) {
            //MAX wins
            string += "COMPUTER WINS!";
        } else if (state.player == 1 && state.numSticks == 1) {
            //MAX loses
            string += "YOU WIN! !";
        } else if (state.player == -1 && state.numSticks == 0) {
            //Min Wins
            string += "YOU WIN!";
        } else if (state.player == -1 && state.numSticks == 1) {
            //Min loses
            string += "COMPUTER WINS!";
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("GAME OVER!");
        alert.setContentText(string);
        alert.showAndWait();

    }

    public void winnerAlert(AlphaBetaState state) {
        String string = "";
        if (state.player == 1 && state.numSticks == 0) {
            //MAX wins
            string += "COMPUTER WINS";
        } else if (state.player == 1 && state.numSticks == 1) {
            //MAX loses
            string += "YOU WIN!";
        } else if (state.player == -1 && state.numSticks == 0) {
            //Min Wins
            string += "YOU WIN!";
        } else if (state.player == -1 && state.numSticks == 1) {
            //Min loses
            string += "COMPUTER WINS!";
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("GAME OVER!");
        alert.setContentText(string);
        alert.showAndWait();

    }

   
}
