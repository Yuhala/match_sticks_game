/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeux_allumettes;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Pito
 */
public class Jeux_Allumettes extends Application {

    public Stage stage1;
    public TextField num;
    public int algo = 1;
    public RadioButton minimax, alphabeta;

    public void start(Stage primaryStage) {

        this.stage1 = primaryStage;
        stage1.setResizable(false);
        GridPane root = new GridPane();        
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(20));
        root.setHgap(15);
        root.setVgap(15);

        //Definitions des styles CSS
        String img = GameStage.class.getResource("/img/CoarseGrid.png").toExternalForm();
        String styleLabel = "-fx-font-family: \"digital-7\";\n"
                + "    -fx-font-size: 18;\n"
                + "    -fx-text-fill: white;\n"
                + "    -fx-effect: dropshadow( one-pass-box , rgba(0, 0, 0, 0.6), 0, 0.0 , 0 , 1 );";
        String paneStyle = "-fx-background-image: url('" + img + "');"
                + "    -fx-background-repeat: repeat;\n"
                + "    -fx-background-color:\n"
                + "        linear-gradient(#38424b 0%, #1f2429 20%, #191d22 100%),\n"
                + "        linear-gradient(#20262b, #191d22),\n"
                + "        radial-gradient(center 50% 0%, radius 100%, rgba(114,131,148,0.9), rgba(255,255,255,0));";
        String algoStyle = "-fx-font-family: \"Arial\";\n"
                + "    -fx-font-size: 18;\n"
                + "    -fx-text-fill: cyan;\n"
                + "    -fx-effect: dropshadow( one-pass-box , rgba(0, 0, 0, 0.6), 0, 0.0 , 0 , 1 );";

        String buttonStyle = "-fx-background-color:\n"
                + "        rgba(255, 255, 255, 0.08),\n"
                + "        rgba(0, 0, 0, 0.8),\n"
                + "        #090a0c,\n"
                + "        linear-gradient(#4a5661 0%, #1f2429 20%, #1f242a 100%),\n"
                + "        linear-gradient(#242a2e, #23282e),\n"
                + "        radial-gradient(center 50% 0%, radius 100%, rgba(135,142,148,0.9),\n"
                + "        rgba(255,255,255,0));\n"
                + "    -fx-background-radius: 7, 6, 5, 4, 3, 5;\n"
                + "    -fx-background-insets: -3 -3 -4 -3, -3, 0, 1, 2, 0;\n"
                + "    -fx-font-family: \"Arial\";\n"
                + "    -fx-text-fill: white;\n"
                + "    -fx-font-size: 20;\n"
                + "    -fx-text-fill: linear-gradient(white, #d0d0d0);\n"
                + "    -fx-padding: 10 20 10 20;";

        root.setStyle(paneStyle);
        Button cont = new Button();
        cont.setStyle(buttonStyle);
        GridPane.setHalignment(cont, HPos.CENTER);
        num = new TextField();

        cont.setText("CONTINUER");
        Label label1 = new Label("NOUVEAU JEUX D'ALLUMETTES");
        label1.setStyle(styleLabel);
        Label label2 = new Label("ENTRER LE NOMBRE D'ALLUMETTES");
        label2.setStyle(styleLabel);

        minimax = new RadioButton("AlGORITHME MINIMAX");
        alphabeta = new RadioButton("AlGORITHME ALPHA-BETA");

        ToggleGroup group = new ToggleGroup();
        minimax.setStyle(algoStyle);
        alphabeta.setStyle(algoStyle);
        minimax.setToggleGroup(group);
        alphabeta.setToggleGroup(group);

        minimax.setOnAction(e -> {
            this.algo = 1;
        });
        alphabeta.setOnAction(e -> {
            this.algo = 2;
        });
        cont.setOnAction(e -> begin());

        root.add(label1, 1, 0);
        root.add(cont, 1, 6);
        root.add(minimax, 1, 2);
        root.add(alphabeta, 1, 3);
        root.add(num, 1, 4);

        Scene scene = new Scene(root, 400, 350);

        stage1.setTitle("Jeux d'Allumettes");
        stage1.setScene(scene);
        stage1.show();
    }

    public void begin() {
        try {
            newGame();
        } catch (Exception e) {
        }
    }

    public void newGame() throws Exception {
        int val = 0;
        val = Integer.parseInt(this.num.getText());
        if (val < 5) {
            errorAlert();

        }else if(val>23){
        timeAlert();
        }
        else {

            new GameStage(stage1, true, new Game(val, algo));
        }

    }

    public void errorAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("MAUVAISE VALEUR!");
        alert.setContentText("ENTRER UNE VALUER >= 5");
        alert.showAndWait();
    }

    public void timeAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ATTENTION!");
        alert.setContentText("La valeur entrée est très grande et va ralentir le jeu! Veuiller entrer un nombre <= 23  ");
        alert.showAndWait();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
