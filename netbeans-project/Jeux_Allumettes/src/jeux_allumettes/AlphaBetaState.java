/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jeux_allumettes;

import java.util.ArrayList;

/**
 *
 * @author Peterson Yuhala
 */
public class AlphaBetaState {
//Un etat AlphaBeta est un etat qui utilise l'algorithm alpha-beta pour generer les differents noeuds fils possibles

    private int minimaxVal;
    public int level;
    public int player;
    public int numSticks;
    public int alpha;
    public int beta;
    public ArrayList<AlphaBetaState> children = new ArrayList<>();

    public AlphaBetaState(int player, int num, int lev) {
        this.player = player;
        this.level = lev;
        this.numSticks = num;
        this.alpha = -100;//On initialise alpha à un nombre tres petit(-infinie)
        this.beta = 100;//On initialise beta à un nombre tres grand(+infini)
        this.addnextStates();
        this.minimaxVal = this.alphaBeta(this, this.player, this.alpha, this.beta);

    }

    public int getMinimaxVal() {
        return this.minimaxVal;
    }

    public int alphaBeta(AlphaBetaState state, int player, int a, int b) {

        //On retourne la valeur de la fonction heuristic pour les etats/noeuds terminaux
        if (state.numSticks == 0 && player == -1) {
            //MIN State
            return -1;
        } else if (state.numSticks == 0 && player == 1) {
            //MAX State
            return 1;
        } else if (state.numSticks == 1 && player == -1) {
            //MIN State
            return 1;
        } else if (state.numSticks == 1 && player == 1) {
            //MAX state
            return -1;
        } else {
            int val, length;
            if (this.player == 1) {
                //Nous sommes sur un noeud MAX

                for (int i = 0; i < this.children.size(); i++) {
                    val = this.children.get(i).alphaBeta(this.children.get(i), -1, a, b);
                    if (val > a) {
                        a = val;
                    }
                    if (a >= b) {
                        //Elegage des autres branches(ie enfants) si alpha >= beta
                       break;
                    }
                }
                return a;
            } else {
                //Nous sommes sur un noeud MIN
                length = this.children.size();
                for (int i = 0; i < length; i++) {
                    val = this.children.get(i).alphaBeta(this.children.get(i), 1, a, b);
                    if (val < b) {
                        b = val;
                    }
                    if (a >= b) {
                        //Elegage des autres branches(ie enfants) si alpha >= beta
                        break;
                        }
                    }
                }
                return b;
            }
        }
    
    
    
     public void addnextStates() {
        int num = this.numSticks;
        boolean test = false;//Le boolean test verifie qu'il n y a pas d'etats égales dans la liste
        int nextplayer, i = 1;//i est un compteur qui verifie que chaque joueur prend un nombre legal d'allumettes(ie 0<n<=3)
        if (this.player == 1) {
            nextplayer = -1;
        } else {
            nextplayer = 1;
        }
        while (num > 0 && i <= 3) {
            num--;
            for (int j = 0; i < this.children.size(); j++) {
                if (this.children.get(j).numSticks == num) {
                    test = true;
                }
            }
            if (test == false) {
                AlphaBetaState newState = new AlphaBetaState(nextplayer, num, this.level + 1);
                this.children.add(newState);
            }

            i++;
        }

    }
}


    

//La fonction addnextStates() ajout les etats suivants possibles à un noeud donné
   /* public void addnextStates() {

        int num = this.numSticks;

        int nextplayer, i = 1;//i is a counter that ensures that at most 3 sticks are taken
        if (this.player == 1) {
            nextplayer = -1;
        } else {
            nextplayer = 1;
        }
        while (num > 0 && i <= 3) {
            num--;

            AlphaBetaState newState = new AlphaBetaState(nextplayer, num, this.level + 1);
            this.children.add(newState);
            i++;
        }

    }
    */
    /*La fonction addnextStates() ajout les etats suivants possibles à un noeud donné
     *Cette fonction aussi ajoute les états manquants(rajoute les états qui ont été élagés dans la liste des états fils 
     * au cas où le joueur MIN ne joue pas de façon optimale.
     */

   