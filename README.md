## Match Sticks AI game using Minimax and Alpha-Beta Pruning
- This is a simple game based on AI algorithms [minimax](https://en.wikipedia.org/wiki/Minimax) and [alpha-beta pruning](https://en.wikipedia.org/wiki/Alpha-beta_pruning).
- The rules of the game are simple: the game begins with a number of match-sticks (preferably < 23 for my case). Each player must pick either 1, 2, or 3 match sticks in turns. The player to pick the last match stick loses the game, i.e if one stick is left and its your turn, you lose :-((!

## How to run the game
- This guide is for linux-based users but the steps are analogous for a windows-based user.
- Prerequisite: Install java on your system.
- Start by cloning the project to your system: `git clone git@gitlab.com:Yuhala/match_sticks_game.git && cd match_sticks_game`. 
- Unzip the JAVA FX SDK in the same directory: `unzip openjfx-11.0.2_linux-x64_bin-sdk.zip` and export the path to your JAVA FX library: `export PATH_TO_FX=$PWD/match_sticks_game/javafx-sdk-11.0.2/lib/`.
- Run the game with: `java -jar --module-path $PATH_TO_FX --add-modules javafx.controls matchsticks.jar `.
- You should see the interface below.

![Box1](./img/box1.png)

- Choose the algorithm: `minimax` or `alpha-beta` and enter the number of match sticks to start the game with: we start with `17` in the picture. 
- Click `continuer`. You should see the next interface below.

![Box2](./img/box2.png).

- Enter the number of sticks you pick and click on `jouer` or play. The computer will pick too and you continue. Whoever picks the last stick loses the game.
- The winner will be shown in the box below:

![Box3](./img/box3.png).

- Happy gaming!!

## P.S
- I developed this game about 4 years ago and was probably not a very good programmer then :-). Will take the time to refactor code during my free time. In the mean-time feel free to inform me on how to make my progam better: petersonyuhala@gmail.com


